import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../onboard_screen/onboard_screen.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 2000), () {
      Get.to(const OnBoardingScreen());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff1D3133),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 157.h,
              ),
              Image.asset('assets/images/logo.png'),
              Padding(
                padding: EdgeInsets.only(top: 211.h, left: 77.w, right: 77.w),
                child: Column(
                  children: [
                    Text(
                      "Tour Viet",
                      style: TextStyle(
                        color: const Color(0xffC1AC67),
                        fontSize: 36.sp,
                        fontFamily: "Abril Fatface",
                        letterSpacing: 10.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Natural Travel",
                      style: TextStyle(
                        color: const Color(0xffFFFFFF),
                        fontSize: 20.sp,
                        fontFamily: "Abyssinica SIL",
                        letterSpacing: 5.sp,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
